
//DECLARATIONS OF BASIC VARIABLES
let clickMarker;
let myObj;
let skyObjects = [];
let visibleStars = []; 
let eclPoints = [];
let links = [];
let standard_stars;
let standard_stars_calculated = [];
let dso = [];
let conFullNames;
let canvasSize = parseInt(window.innerHeight) + 40;
document.getElementById("canvas").width = window.innerHeight + 40;
document.getElementById("canvas").height = document.getElementById("canvas").width;
prepareForDrawing();



//HTML INTERFACE AND MAP FUNCTIONS
document.getElementById("submit").addEventListener("click", main);
document.getElementById("submit").addEventListener("input", main);

document.getElementById("now").addEventListener("click", function(){
	let temp = new Date()
	offset = temp.getTimezoneOffset()
	let d = new Date(temp.getTime() + offset*60*1000);

	let date = [d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes()];
	date[1]++;
	for(let i = 1; i < 5; i++)
	{
		if(date[i]<10)
		{
			date[i] = "0"+date[i];
		}
	}
	document.getElementById("date").value = date[0] + "-" + date[1] + "-" + date[2];
	document.getElementById("time").value = date[3] + ":" + date[4];
});

document.getElementById("lat").addEventListener("change", function(){
clickMarker.setMap(null);
});

document.getElementById("lng").addEventListener("change", function(){
clickMarker.setMap(null);
});

document.getElementById("star-range").addEventListener("input", function(){
document.getElementById("star-range-text").innerHTML = 
document.getElementById("star-range").value;
});

document.getElementById("starname-range").addEventListener("input", function(){
document.getElementById("starname-range-text").innerHTML = 
document.getElementById("starname-range").value;
});

document.getElementById("conname-range").addEventListener("input", function(){
switch(parseInt(document.getElementById("conname-range").value))
{
	case 0:
		document.getElementById("conname-range-text").innerHTML = "No names";
		break;
	case 1:
		document.getElementById("conname-range-text").innerHTML = "Big";
		break;
	case 2:
		document.getElementById("conname-range-text").innerHTML = "Medium+";
		break;
	case 3:
		document.getElementById("conname-range-text").innerHTML = "Small+";
		break;
	case 4:
		document.getElementById("conname-range-text").innerHTML = "All";
		break;
}

});

document.getElementById("map-show").addEventListener("click", function(){
document.getElementById("show").style = "display:block";
});

function initMap() {
let cords = {lat: 52, lng: -20};
let map = new google.maps.Map(document.getElementById('map'), {
zoom: 3,
center: cords
});
clickMarker = new google.maps.Marker({
	position: cords,
	map: null
})

map.addListener('click', function(e){
  clickMarker.setMap(null);

  readCords(e.latLng, map);
});
}

function readCords(cords, map)
{
	document.getElementById("lat").value = cords.lat().toFixed(4);
	document.getElementById("lng").value = cords.lng().toFixed(4);

	clickMarker = new google.maps.Marker({
		position: cords,
		map: map
	});
}

//DATA LOADING
xhttpRequest("php/data.php", loadData);
xhttpRequest("php/starlinks.php", loadStarlinks);
xhttpRequest("php/standard.php", loadStandard);
xhttpRequest("php/dso.php", loadDSO);

function xhttpRequest(url, callback)//sending to server and receiving info
{
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function()
	{
		if(this.readyState == 4 && this.status == 200)
		{
			if(callback != false)
			{
				return callback(this);
			}
		}
	} 
	xhttp.open("POST", url , true);
	xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	let vars = "";
	for(let i = 2; i < arguments.length; i+=2)
	{
		vars+=xhttpRequest.arguments[i]+"="+xhttpRequest.arguments[i+1]
		if(i+2 < arguments.length)
		{
			vars+="&"
		}
	}
	xhttp.send(vars);

}

function loadData(xhttp)
{
	myObj = JSON.parse(xhttp.responseText);
}

function loadStarlinks(xhttp)
{
	links = JSON.parse(xhttp.responseText);
}

function loadStandard(xhttp)
{
	standard_stars = JSON.parse(xhttp.responseText);
}

function loadDSO(xhttp)
{
	let dso_loaded = JSON.parse(xhttp.responseText);
	dso_loaded.forEach(el => {
		ID1 = el.ID1;
		ra = parseFloat(el.rah) + parseFloat(el.ram)/60 + parseFloat(el.ras)/3600;
		if(el.decsign == "+"){
			dec = parseFloat(el.decd) + parseFloat(el.decm)/60 + parseFloat(el.decs)/3600;
		}else{
			dec = -1*(parseFloat(el.decd) + parseFloat(el.decm)/60 + parseFloat(el.decs)/3600);
		}
		dso.push({ID1,ra, dec});
	});
}

//MAIN FUNCTION
function main(){
	clearMap()
	document.getElementById("canvas-parent").style = "display:block";
	
	xhttpRequest("polldata.xml", function(xml){
		lightPollution(xml, document.getElementById("lat").value, 
				document.getElementById("lng").value);
	});

	xhttpRequest("https://api.openweathermap.org/data/2.5/weather?lat="
	+document.getElementById("lat").value 
	+"&lon="+document.getElementById("lng").value 
	+"&APPID=ff3fe125b3c9408d921ea809713aedbb", processWeather);

	

	let d = document.getElementById("date").value.split("-");
	let h = document.getElementById("time").value.split(":");
	let date =	Date.UTC(d[0], d[1]-1, d[2], h[0], h[1], 0);
	let UT = (h[0] - (-h[1]/60))/24;
	
	visibleStars = [];
	for (let x in myObj) {
		convertEquatorialToHorizontal(
			myObj[x].ra,
			myObj[x].decl,
			document.getElementById("lat").value,
			document.getElementById("lng").value,
			myObj[x].mag,
			myObj[x].proper,
			myObj[x].con,
			myObj[x].hd,
			date);
	}
	if(document.getElementById('ecl-check').checked == true){
		drawEcliptic(document.getElementById("lat").value, document.getElementById("lng").value, date, UT)
	}

	countLinks();

	if(document.getElementById('links-check').checked == true){
		drawLinks();
	}
	if(document.getElementById('stand-check').checked == true){
		calculateStandardStars(date, document.getElementById("lat").value, document.getElementById("lng").value);
		drawStandardStars();
	}

	if(document.getElementById('dso-check').checked == true){
		drawDSO(date, document.getElementById("lat").value, document.getElementById("lng").value);
	}

	for (let x in visibleStars) {
		drawStar(visibleStars[x]);
	}

	drawConName();

	for (let x in visibleStars) {
		drawName(visibleStars[x]);
	}

	if(document.getElementById('solarsys-check').checked == true){
		drawSolarSystem(document.getElementById("lat").value, document.getElementById("lng").value, date, UT)
	}
	
	clearEdges();
};

//DRAW FUNCTIONS
function clearMap(){
	let c = document.getElementById("canvas");
	let ctx = c.getContext("2d");

	ctx.strokeStyle="white";
	ctx.fillStyle="white";

	ctx.clearRect ( -canvasSize/2 , -canvasSize/2 , canvasSize , canvasSize );

	ctx.beginPath();
	ctx.arc(0, 0, canvasSize/2-30, 0, 2*Math.PI);
	ctx.stroke();		
}
function prepareForDrawing()
{
	let c = document.getElementById("canvas");
	let ctx = c.getContext("2d");
	ctx.translate(canvasSize/2, canvasSize/2);
}

function drawEcliptic(lat, lng, date, UT){
	let c = document.getElementById("canvas");
	let ctx = c.getContext("2d");
	let d = Math.floor((date - Date.UTC(2000, 0, 1))/1000/60/60/24); 
	d = d+ UT;
	let orb_el = new Array(6)
	
	
	let sun_ecl = [];
	for(let i = d; i < d+367; i +=1/4){
		let ecl = (23.4393 - Math.pow(3.563, -7) * i)/180*Math.PI;
		
		orb_el[0] = 0.0;
		orb_el[1] = 0.0;
		orb_el[2] = ((282.9404 + 4.70935E-5 * i) )/180*Math.PI;
		orb_el[3] = 1.000000;
		orb_el[4] = 0.016709 - 1.151E-9 * i;
		orb_el[5] = ((356.0470 + 0.9856002585 * i)%360)/180*Math.PI;
		
		
		let E = orb_el[5] + orb_el[4] * Math.sin(orb_el[5]) * (1.0 + orb_el[4] * Math.cos(orb_el[5]));
		let Xvs = Math.cos(E) - orb_el[4];
		let Yvs = Math.sqrt(1.0 - orb_el[4] * orb_el[4]) * Math.sin(E);
		let vs = Math.atan2(Yvs, Xvs);
		let rs = Math.sqrt(Xvs * Xvs + Yvs * Yvs);
		let lonsun = vs + orb_el[2];
		let Xs = rs * Math.cos(lonsun);
		let Ys = rs * Math.sin(lonsun);

		let Xe = Xs;
		let Ye = Ys * Math.cos(ecl);
		let Ze = Ys * Math.sin(ecl);
		let RA  = Math.atan2(Ye, Xe) + 2*Math.PI;
		let Decl = Math.atan2(Ze, Math.sqrt(Xe * Xe + Ye * Ye));
		sun_ecl.push({RA, Decl})
		
	}

	orb_el[0] = 0.0;
	orb_el[1] = 0.0;
	orb_el[2] = ((282.9404 + 4.70935E-5 * d) )/180*Math.PI;
	orb_el[3] = 1.000000;
	orb_el[4] = 0.016709 - 1.151E-9 * d;
	orb_el[5] = ((356.0470 + 0.9856002585 * d)%360)/180*Math.PI;
	let Ls = orb_el[2] + orb_el[5];
	let GMST0 = Ls/Math.PI*180 / 15 + 12; 
	let GMST = GMST0 + UT*24;
	let LST = GMST + lng / 15
	for(let i in sun_ecl)
	{
		let HA = (LST%24)/12*Math.PI - sun_ecl[i].RA
		if(HA > Math.PI) HA = HA - 2*Math.PI
		if(HA < -1*Math.PI) HA = HA + 2*Math.PI

		let X = Math.cos(HA) * Math.cos(sun_ecl[i].Decl)
		let Y = Math.sin(HA) * Math.cos(sun_ecl[i].Decl)
		let Z = Math.sin(sun_ecl[i].Decl)
		let Xhor = X * Math.sin(lat/180*Math.PI) - Z * Math.cos(lat/180*Math.PI)
		let Yhor = Y
		let Zhor = X * Math.cos(lat/180*Math.PI) + Z * Math.sin(lat/180*Math.PI)
		let AZ = Math.atan2(Yhor, Xhor) + Math.PI
		let ALT = Math.asin(Zhor) 

		sun_ecl.push({AZ, ALT})
		if(ALT > 0){
			let radius = (canvasSize/2-25)*((90 - ALT/Math.PI*180)/90);
			let ecliptic_color = "#ffba30";//ffc507
			let xdraw = radius * Math.cos(AZ+Math.PI/2); 
			let ydraw = radius * Math.sin(AZ+Math.PI/2); 
			ctx.strokeStyle=ecliptic_color;
			ctx.fillStyle=ecliptic_color;
			ctx.beginPath();
			ctx.arc(xdraw, -ydraw,canvasSize/(1000)* (0.5), 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
		}
	}


	
}

function drawSolarSystem(lat, lng, date, UT){
	let c = document.getElementById("canvas");
	let ctx = c.getContext("2d");
	let d = Math.floor((date - Date.UTC(2000, 0, 1))/1000/60/60/24); 
	d = d+ UT;

	let ecl = (23.4393 - Math.pow(3.563, -7) * d)/180*Math.PI;
	var orb_el = new Array(9);

	for (var i = 0; i < orb_el.length; i++) {
		orb_el[i] = new Array(6);//N i w a e M
	}
	let planet_names = ['Sun', 'Mercury', 'Venus', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptun','Moon'];
	let planet_mag = [-6,-1.5,-4,-2.5,-2.5,-0.5,5,7,-5]

	orb_el[0][0] = 0.0;
    orb_el[0][1] = 0.0;
    orb_el[0][2] = ((282.9404 + 4.70935E-5 * d) )/180*Math.PI;
    orb_el[0][3] = 1.000000;
    orb_el[0][4] = 0.016709 - 1.151E-9 * d;
    orb_el[0][5] = ((356.0470 + 0.9856002585 * d)%360)/180*Math.PI;

	orb_el[1][0] = ((48.3313 + 3.24587E-5 * d) )/180*Math.PI;
    orb_el[1][1] = ((7.0047 + 5.00E-8 * d) )/180*Math.PI;
    orb_el[1][2] = ((29.1241 + 1.01444E-5 * d) )/180*Math.PI;
    orb_el[1][3] = 0.387098;   
    orb_el[1][4] = 0.205635 + 5.59E-10 * d;
	orb_el[1][5] = ((168.6562 + 4.0923344368 * d)%360)/180*Math.PI;

	orb_el[2][0] =((  76.6799 + 2.46590E-5 * d) )/180*Math.PI
    orb_el[2][1] =(( 3.3946 + 2.75E-8 * d) )/180*Math.PI
    orb_el[2][2] =((  54.8910 + 1.38374E-5 * d) )/180*Math.PI
    orb_el[2][3] = 0.723330  
    orb_el[2][4] = 0.006773 - 1.302E-9 * d
    orb_el[2][5] =((  48.0052 + 1.6021302244 * d) )/180*Math.PI

    orb_el[3][0] =  ((49.5574 + 2.11081E-5 * d) )/180*Math.PI
    orb_el[3][1] = ((1.8497 - 1.78E-8 * d) )/180*Math.PI
    orb_el[3][2] = ((286.5016 + 2.92961E-5 * d) )/180*Math.PI
    orb_el[3][3] = 1.523688  
    orb_el[3][4] = 0.093405 + 2.516E-9 * d
    orb_el[3][5] =  ((18.6021 + 0.5240207766 * d) )/180*Math.PI
	
	orb_el[4][0] = ((100.4542 + 2.76854E-5 * d) )/180*Math.PI
    orb_el[4][1] = ((1.3030 - 1.557E-7 * d) )/180*Math.PI
    orb_el[4][2] = ((273.8777 + 1.64505E-5 * d) )/180*Math.PI
    orb_el[4][3] = 5.22525   
    orb_el[4][4] = 0.048498 + 4.469E-9 * d
    orb_el[4][5] = ((19.8950 + 0.0830853001 * d)%360)/180*Math.PI
	

    orb_el[5][0] = ((113.6634 + 2.38980E-5 * d) )/180*Math.PI
    orb_el[5][1] = ((2.4886 - 1.081E-7 * d) )/180*Math.PI
    orb_el[5][2] = ((339.3939 + 2.97661E-5 * d) )/180*Math.PI
    orb_el[5][3] = 9.55475  
    orb_el[5][4] = 0.055546 - 9.499E-9 * d
    orb_el[5][5] = ((316.9670 + 0.0334442282 * d) )/180*Math.PI

    orb_el[6][0] =  ((74.0005 + 1.3978E-5 * d) )/180*Math.PI
	orb_el[6][1] = ((0.7733 + 1.9E-8 * d) )/180*Math.PI
	orb_el[6][2] =  ((96.6612 + 3.0565E-5 * d) )/180*Math.PI
	orb_el[6][3] = 19.18171 - 1.55E-8 * d 
	orb_el[6][4] = 0.047318 + 7.45E-9 * d
	orb_el[6][5] = ((142.5905 + 0.011725806 * d) )/180*Math.PI

	orb_el[7][0] = ((131.7806 + 3.0173E-5 * d) )/180*Math.PI
    orb_el[7][1] = ((1.7700 - 2.55E-7 * d) )/180*Math.PI
    orb_el[7][2] = ((272.8461 - 6.027E-6 * d) )/180*Math.PI
    orb_el[7][3] = 30.05826 + 3.313E-8 * d  
    orb_el[7][4] = 0.008606 + 2.15E-9 * d
	orb_el[7][5] = ((260.2471 + 0.005995147 * d) )/180*Math.PI
	
	orb_el[8][0] = ((125.1228 - 0.0529538083 * d) )/180*Math.PI
    orb_el[8][1] = (5.1454)/180*Math.PI
    orb_el[8][2] = ((318.0634 + 0.1643573223 * d) )/180*Math.PI
    orb_el[8][3] = 60.2666  
    orb_el[8][4] = 0.0549
    orb_el[8][5] = ((115.3654 + 13.0649929509 * d) )/180*Math.PI

	let Ls = orb_el[0][2] + orb_el[0][5];
	let GMST0 = Ls/Math.PI*180 / 15 + 12; 
	let GMST = GMST0 + UT*24;
	let LST = GMST + lng / 15
	let E = orb_el[0][5] + orb_el[0][4] * Math.sin(orb_el[0][5]) * (1.0 + orb_el[0][4] * Math.cos(orb_el[0][5]));
	let Xvs = Math.cos(E) - orb_el[0][4];
	let Yvs = Math.sqrt(1.0 - orb_el[0][4] * orb_el[0][4]) * Math.sin(E);
	let vs = Math.atan2(Yvs, Xvs);
	let rs = Math.sqrt(Xvs * Xvs + Yvs * Yvs	);
	let lonsun = vs + orb_el[0][2];
	let Xs = rs * Math.cos(lonsun);
	let Ys = rs * Math.sin(lonsun);

	//Sun
	let Xe = Xs;
	let Ye = Ys * Math.cos(ecl);
	let Ze = Ys * Math.sin(ecl);
	let RA  = Math.atan2(Ye, Xe) + 2*Math.PI
	let Decl = Math.atan2(Ze, Math.sqrt(Xe * Xe + Ye * Ye))
	let HA = (LST%24)/12*Math.PI - RA
	if(HA > Math.PI) HA = HA - 2*Math.PI
	if(HA < -1*Math.PI) HA = HA + 2*Math.PI

	let X = Math.cos(HA) * Math.cos(Decl)
	let Y = Math.sin(HA) * Math.cos(Decl)
	let Z = Math.sin(Decl)
	let Xhor = X * Math.sin(lat/180*Math.PI) - Z * Math.cos(lat/180*Math.PI)
	let Yhor = Y
	let Zhor = X * Math.cos(lat/180*Math.PI) + Z * Math.sin(lat/180*Math.PI)
	let AZ = Math.atan2(Yhor, Xhor) + Math.PI
	let ALT = Math.asin(Zhor) 

	if(ALT > 0){
		let radius = (canvasSize/2-25)*((90 - ALT/Math.PI*180)/90);
		let planet_color = "#ffc507";
		let xdraw = radius * Math.cos(AZ+Math.PI/2); 
		let ydraw = radius * Math.sin(AZ+Math.PI/2); 
		ctx.strokeStyle=planet_color;
		ctx.fillStyle=planet_color;
		ctx.beginPath();
		ctx.arc(xdraw, -ydraw,canvasSize/(1000)* (5 - (planet_mag[0]*0.6)), 0, 2*Math.PI);
		ctx.fill();
		ctx.stroke();
		
		ctx.strokeStyle="white";
		ctx.fillStyle="white";
		
		ctx.beginPath();
		ctx.font = "14px Serif";
		ctx.textBaseline = 'top';
		ctx.fillStyle = '#000000';  
		let width = ctx.measureText("Sun").width;
		ctx.fillRect(xdraw-20, -ydraw+10, width, parseInt(ctx.font, 10));

		ctx.fillStyle = planet_color;
		ctx.fillText("Sun",xdraw-20,-ydraw+10);
	}
	
	
	
	//Planets
	for (var i = 1; i < 9; i++) { //N i w a e M
		let E = orb_el[i][5] + orb_el[i][4]  * Math.sin(orb_el[i][5]) * (1.0 + orb_el[i][4] * Math.cos(orb_el[i][5]));
		let Xv = orb_el[i][3] * (Math.cos(E) - orb_el[i][4]);
		let Yv = orb_el[i][3] * (Math.sqrt(1.0 - orb_el[i][4] * orb_el[i][4]) * Math.sin(E));
		let v = Math.atan2(Yv, Xv);
		let r = Math.sqrt(Xv * Xv + Yv * Yv);
		let Xh = r * (Math.cos(orb_el[i][0]) * Math.cos(v + orb_el[i][2]) - Math.sin(orb_el[i][0]) * Math.sin(v + orb_el[i][2]) * Math.cos(orb_el[i][1]))
		let Yh = r * (Math.sin(orb_el[i][0]) * Math.cos(v + orb_el[i][2]) + Math.cos(orb_el[i][0]) * Math.sin(v + orb_el[i][2]) * Math.cos(orb_el[i][1]))
		let Zh = r * (Math.sin(v + orb_el[i][2]) * Math.sin(orb_el[i][1]))
		let lonecl = Math.atan2(Yh, Xh) +  2 * Math.PI
		let latecl = Math.atan2(Zh, Math.sqrt(Xh * Xh + Yh * Yh)) 
		Xh = r * Math.cos(lonecl) * Math.cos(latecl)
		Yh = r * Math.sin(lonecl) * Math.cos(latecl)
		Zh = r * Math.sin(latecl)
		let Xg = Xh + Xs
		let Yg = Yh + Ys
		let Zg = Zh
		if(i == 8){
			Xg = Xh;
			Yg = Yh;
			Zg = Zh;
		}
		 Xe = Xg;
		 Ye = Yg * Math.cos(ecl) - Zg * Math.sin(ecl)
		 Ze = Yg * Math.sin(ecl) + Zg * Math.cos(ecl)
		 RA  = Math.atan2(Ye, Xe) + 2*Math.PI
		 Decl = Math.atan2(Ze, Math.sqrt(Xe * Xe + Ye * Ye))
		 HA = (LST%24)/12*Math.PI - RA
		if(HA > Math.PI) HA = HA - 2*Math.PI
		if(HA < -1*Math.PI) HA = HA + 2*Math.PI

		 X = Math.cos(HA) * Math.cos(Decl)
		 Y = Math.sin(HA) * Math.cos(Decl)
		 Z = Math.sin(Decl)
		 Xhor = X * Math.sin(lat/180*Math.PI) - Z * Math.cos(lat/180*Math.PI)
		 Yhor = Y
		 Zhor = X * Math.cos(lat/180*Math.PI) + Z * Math.sin(lat/180*Math.PI)

		 AZ = Math.atan2(Yhor, Xhor) + Math.PI
		 ALT = Math.asin(Zhor) 

		if(ALT > 0){
			 radius = (canvasSize/2-25)*((90 - ALT/Math.PI*180)/90);
			 planet_color = "#00e1ff "
			 xdraw = radius * Math.cos(AZ+Math.PI/2); 
			 ydraw = radius * Math.sin(AZ+Math.PI/2); 
			ctx.strokeStyle=planet_color;
			ctx.fillStyle=planet_color;
			if(i==8){
				ctx.strokeStyle='#cecece';
				ctx.fillStyle = '#cecece';
			}
			ctx.beginPath();
			ctx.arc(xdraw, -ydraw,canvasSize/(1000)* (5 - (0.6*planet_mag[i])), 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
			
			ctx.strokeStyle="white";
			ctx.fillStyle="white";
			
			ctx.beginPath();
			ctx.font = "14px Serif"; 	
			ctx.textBaseline = 'top';
			ctx.fillStyle = '#000000';
			 width = ctx.measureText(planet_names[i]).width;
			ctx.fillRect(xdraw-20, -ydraw+10, width, parseInt(ctx.font, 10));
			
			ctx.fillStyle = planet_color;  
			if(i==8){
				ctx.strokeStyle='#cecece';
				ctx.fillStyle = '#cecece';
			}
			ctx.fillText(planet_names[i],xdraw-20,-ydraw+10);
		}
		
	}
	



}

function drawStar(star)
{
	let c = document.getElementById("canvas");
	let ctx = c.getContext("2d");
	ctx.strokeStyle="white";
	ctx.fillStyle="white";
	ctx.beginPath();
	ctx.arc(star.x, -star.y,canvasSize/(1000)* (4 - (0.6*star.mag)), 0, 2*Math.PI);
	ctx.fill();
	ctx.stroke();
}

function calculateStandardStars(time, lat, lng){
	standard_stars_calculated = [];
	standard_stars.forEach(el => {
		let h_ha = ((time/1000- 946728000)/24/3600 * 24.06570982441908 + 18.697374558)%24 + lng/15  - el.ra;
		let ha = h_ha * 15 * 2 * Math.PI/360; 
		let decl = el.decl * 2*Math.PI/360;
		lat = lat * 2*Math.PI/360;
		let temp1 = Math.sin(decl) * Math.sin(lat)  + Math.cos(decl) * Math.cos(lat) * Math.cos(ha);
		let temp2 = Math.sin(decl) * Math.cos(lat) - Math.cos (decl) * Math.sin(lat) * Math.cos(ha);
		let temp3 =  - (Math.cos(decl) * Math.sin(ha));

		let alt = Math.atan2(temp1, Math.sqrt(temp2*temp2+temp3*temp3))/(2*Math.PI)*360 ;

		let az = (Math.atan2(temp3, temp2)/(2*Math.PI)*360 + 360)%360;
		let starname = el.starname;
		let V = el.V; let B = el.B; let R = el.B; let U = el.U
		standard_stars_calculated.push({starname, az, alt, V, B, R, U});
	});
}

function drawStandardStars(){
	standard_stars_calculated.forEach(el => {
		if(el.alt > 0){
			let radius = (canvasSize/2-25)*((90 - el.alt)/90);

			let x = radius * Math.cos(el.az * 2*Math.PI/360+Math.PI/2); 
			let y = radius * Math.sin(el.az * 2*Math.PI/360+Math.PI/2); 

			let c = document.getElementById("canvas");
			let ctx = c.getContext("2d");
			ctx.strokeStyle="#53ff23";
			ctx.fillStyle="#53ff23";
			ctx.beginPath();
			ctx.arc(x, -y,canvasSize/(1000)* 2, 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
		}
		
	});
}

function drawName(star)
{
	if(star.mag<parseFloat(document.getElementById("starname-range").value)
	 && star.hd != 122451)
	{
		let c = document.getElementById("canvas");
		let ctx = c.getContext("2d");

		ctx.strokeStyle="white";
		ctx.fillStyle="white";
		ctx.beginPath();
		ctx.font = "14px Serif";
		ctx.textBaseline = 'top';
		ctx.fillStyle = '#000000';  
		let width = ctx.measureText(star.proper).width;
		ctx.fillRect(star.x-20, -star.y+10, width, parseInt(ctx.font, 10));

		ctx.fillStyle = '#ffffff';
		ctx.fillText(star.proper,star.x-20,-star.y+10);
	}
}

function clearEdges(){
	let c = document.getElementById("canvas");
	let ctx = c.getContext("2d");

	ctx.fillStyle="#000000";
	ctx.strokeStyle='#ffffff';
			
	ctx.beginPath();
	ctx.moveTo(-canvasSize/2, -canvasSize/2);
	ctx.lineTo(canvasSize/2, -canvasSize/2);
	ctx.lineTo(canvasSize/2,canvasSize/2);
	ctx.lineTo(-canvasSize/2,canvasSize/2);
	ctx.lineTo(-canvasSize/2,-canvasSize/2);
	ctx.closePath();
	ctx.moveTo(canvasSize/2-30 ,0);
	ctx.arc(0, 0, canvasSize/2-30, 0, 2*Math.PI, true);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();

	ctx.font = '25px serif';
	ctx.fillStyle = '#ff0000';
  	ctx.fillText('N', 0, -canvasSize/2+5);
  	ctx.fillText('W', canvasSize/2-27, 0);
  	ctx.fillText('S', 0, canvasSize/2-25);
  	ctx.fillText('E', -canvasSize/2+5, 0);
}

function drawLinks()
{
	if(links.length != undefined)
	{
		for (let x = 0; x < links.length; x++) 
		{
	       	let c = document.getElementById("canvas");
			let ctx = c.getContext("2d");
			let cords1 = visibleStars.find(checkStar1, links[x]); 
			let cords2 = visibleStars.find(checkStar2, links[x]); 

			if(cords1 != undefined && cords2 != undefined)
			{
				ctx.beginPath();
				ctx.strokeStyle = '#ff0800';
				ctx.moveTo(cords1.x,-1*cords1.y);
				ctx.lineTo(cords2.x,-1*cords2.y);
				ctx.stroke();
			}
	    }
	}
}

function drawConName()
{
	let doneCons = [];
	for (let x = 0; x < links.length; x++) 
	{
       	let c = document.getElementById("canvas");
		let ctx = c.getContext("2d");
		let cords1 = visibleStars.find(checkStar1, links[x]); 
		let cords2 = visibleStars.find(checkStar2, links[x]); 

		if(cords1 != undefined && cords2 != undefined)
		{
			if(doneCons.find(checkCon, links[x].con) == undefined 
				&& ((links[x].beg == linesWithConNames[links[x].con][0]&& links[x].ending ==linesWithConNames[links[x].con][1]) 
			    || (links[x].ending == linesWithConNames[links[x].con][0]&& links[x].beg ==linesWithConNames[links[x].con][1])))
			{
				doneCons.push(links[x].con);
			    if(conNumberOfLinks[links[x].con] > sizeOfCons['s'+document.getElementById("conname-range").value])
			    {
			    	ctx.font = "15px Serif";
				    ctx.textBaseline = 'top';
				    ctx.fillStyle = '#000000';  

					let width = ctx.measureText(conLatinFullNames[links[x].con]).width;
			    	ctx.fillRect(cords1.x, -1*cords1.y +10, width, parseInt(ctx.font, 10));

			    	ctx.fillStyle = "yellow";
					ctx.fillText(conLatinFullNames[links[x].con],cords1.x  ,-1*cords1.y +10);
			    }
			    conNumberOfLinks[links[x].con]=0;
				
				
			}
		}


    }
}

function drawDSO(date, lat, lng){
	let dso_calculated = [];
	dso.forEach(el => {
		let h_ha = ((date/1000- 946728000)/24/3600 * 24.06570982441908 + 18.697374558)%24 + lng/15  - el.ra;
		let ha = h_ha * 15 * 2 * Math.PI/360; 
		let decl = el.dec * 2*Math.PI/360;
		let latx = lat * 2*Math.PI/360;

		let temp1 = Math.sin(decl) * Math.sin(latx)  + Math.cos(decl) * Math.cos(latx) * Math.cos(ha);
		let temp2 = Math.sin(decl) * Math.cos(latx) - Math.cos (decl) * Math.sin(latx) * Math.cos(ha);
		let temp3 =  - (Math.cos(decl) * Math.sin(ha));

		let alt = Math.atan2(temp1, Math.sqrt(temp2*temp2+temp3*temp3))/(2*Math.PI)*360 ;

		let az = (Math.atan2(temp3, temp2)/(2*Math.PI)*360 + 360)%360;
		let name = el.ID1;


		dso_calculated.push({name, az, alt});
	});
	console.log(dso_calculated)
	dso_calculated.forEach(el => {
		if(el.alt > 0){
			let radius = (canvasSize/2-25)*((90 - el.alt)/90);

			let x = radius * Math.cos(el.az * 2*Math.PI/360+Math.PI/2); 
			let y = radius * Math.sin(el.az * 2*Math.PI/360+Math.PI/2); 

			let c = document.getElementById("canvas");
			let ctx = c.getContext("2d");

			/*KROPKI DLA GALAKTYK
			ctx.strokeStyle="#ff8800";
			ctx.fillStyle="#ff8800";
			ctx.beginPath();
			ctx.rect(x, -y, canvasSize/(1000)* 5,  canvasSize/(1000)* 5);
			ctx.fill();
			ctx.stroke();*/

		
			ctx.font = "10px Serif";
			ctx.textBaseline = 'top';
			ctx.fillStyle = '#ff8800';
			ctx.fillText(el.name,x,-y);
		}
		
	});
}

//UTILITY AND HELP FUNCTIONS
function convertEquatorialToHorizontal(ra, dec, lat, lng, mag, proper, con, hd, time)
{
	let h_ha = ((time/1000- 946728000)/24/3600 * 24.06570982441908 + 18.697374558)%24 + lng/15  - ra;
	let ha = h_ha * 15 * 2 * Math.PI/360; //hours to rads
	dec = dec * 2*Math.PI/360;
	lat = lat * 2*Math.PI/360;

	let temp1 = Math.sin(dec) * Math.sin(lat)  + Math.cos(dec) * Math.cos(lat) * Math.cos(ha);
	let temp2 = Math.sin(dec) * Math.cos(lat) - Math.cos (dec) * Math.sin(lat) * Math.cos(ha);
	let temp3 =  - (Math.cos(dec) * Math.sin(ha));

	let altitude = Math.atan2(temp1, Math.sqrt(temp2*temp2+temp3*temp3))/(2*Math.PI)*360 ;

	let azimuth = (Math.atan2(temp3, temp2)/(2*Math.PI)*360 + 360)%360 ;

	addVisStars(azimuth, altitude, mag, proper, con, hd);
}

function addVisStars(az, alt, mag, proper, con, hd)
{
	let radius = (canvasSize/2-25)*((90 - alt)/90);

	let x = radius * Math.cos(az * 2*Math.PI/360+Math.PI/2); 
	let y = radius * Math.sin(az * 2*Math.PI/360+Math.PI/2); 

	if(alt>-5 && mag >-2 && mag<=document.getElementById("star-range").value)
	{
		visibleStars.push({con, hd, x, y, mag, proper});
	}
}

function checkStar1(element)
{
	if( element.hd == this.beg)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function checkStar2(element)
{
	if( element.hd == this.ending)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function checkCon(element)
{
	if( element == this)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function countLinks()
{
	if(links.length != undefined)
	{
		for (let x = 0; x < links.length; x++) 
		{
	       	let c = document.getElementById("canvas");
			let ctx = c.getContext("2d");
			let cords1 = visibleStars.find(checkStar1, links[x]); 
			let cords2 = visibleStars.find(checkStar2, links[x]); 

			if(cords1 != undefined && cords2 != undefined)
			{
				conNumberOfLinks[links[x].con] += 1;	
			}
	    }
	}
}

//LANDOLT STANDARD PHOTOMETRIC STARS CALCULATOR DISPLAY

document.getElementById('stand-button').addEventListener('click', function(){
	infoStandardStar(document.getElementById('stand-name').value)
});

function infoStandardStar(name){
	let d = document.getElementById("date").value.split("-");
	let h = document.getElementById("time").value.split(":");
	let date =	Date.UTC(d[0], d[1]-1, d[2], h[0], h[1], 0);

	calculateStandardStars(date, document.getElementById("lat").value, document.getElementById("lng").value);
	console.log()
	let star = standard_stars_calculated.find(checkName, name);
	if(star != undefined){
		document.getElementById("stand-display").innerHTML = "Height: "+star.alt.toFixed(2) + " degrees<br>Azimuth: "+ star.az.toFixed(2)+" degrees<br>"+"Fotometry V/B/R/U: "+star.V+"/"+star.B+"/"+star.R+"/"+star.U;
	}else{
		document.getElementById("stand-display").innerHTML = "Star not found!";
	}
	if(star.alt > 0){
		let radius = (canvasSize/2-25)*((90 - star.alt)/90);

		let x = radius * Math.cos(star.az * 2*Math.PI/360+Math.PI/2); 
		let y = radius * Math.sin(star.az * 2*Math.PI/360+Math.PI/2); 

		let c = document.getElementById("canvas");
		let ctx = c.getContext("2d");
		ctx.strokeStyle="#53ff23";
		ctx.fillStyle="#53ff23";
		ctx.beginPath();
		ctx.arc(x, -y,canvasSize/(1000)* 8, 0, 2*Math.PI);
		ctx.fill();
		ctx.stroke();
	}
	
}

function checkName(element){
	if(element.starname == this){
		return true;
	}
}


//WEATHER 
function processWeather(xhttp)
{
	let weatherTab = JSON.parse(xhttp.responseText);
	let clouds = weatherTab.clouds.all;
	document.getElementById("weather").innerHTML = "Weather: <b style='color: #919eff'>"+clouds+"% clouds<br>"

	if(clouds>80)
	{
		document.getElementById("weather").innerHTML += "Overcast";
	}
	else if(clouds>50)
	{
		document.getElementById("weather").innerHTML += "A lot of clouds";
	}
	else if(clouds>20)
	{
		document.getElementById("weather").innerHTML += "A little cloudy";
	}
	else 
	{
		document.getElementById("weather").innerHTML += "Clear sky";
	}
}

//LIGHT POLLUTION 
function lightPollution(xml, lat, lon)
{
	let rows = xml.responseXML.getElementsByTagName("row")
	for(let i = 0; i <rows.length; i++)
	{
		let north = parseFloat(rows[i].getElementsByTagName("FIELD4")[0].childNodes[0].nodeValue) ;
		let south = parseFloat(rows[i].getElementsByTagName("FIELD5")[0].childNodes[0].nodeValue) ;
		let east = parseFloat(rows[i].getElementsByTagName("FIELD6")[0].childNodes[0].nodeValue) ;
		let west = parseFloat(rows[i].getElementsByTagName("FIELD7")[0].childNodes[0].nodeValue) ;
		if(lat<north && lat>south && lon < east && lon > west )
		{
			let y = parseInt(1024 - ( 1024/(Math.abs(north-south)) * Math.abs(lat-south) ));
			let x = parseInt(1005/(Math.abs(east-west)) * Math.abs(lon-west));
			
			var img = new Image();
			img.src = 'data/'+rows[i].getElementsByTagName("FIELD3")[0].childNodes[0].nodeValue;
			console.log(img.src);
			var canvas = document.getElementById('test');
			canvas.width = 1005;
			canvas.height = 1024
			var ctx = canvas.getContext('2d');
			img.onload = function() {
			  	ctx.drawImage(img, 0, 0);
			 	img.style.display = 'none';
			 	
			 	let pixel = ctx.getImageData(x, y, 5, 5);
				let data = pixel.data;
				let t1=0, t2=0, t3=0;
				for(let i = 0; i<25;i++)
				{
					t1+= data[i*4];
					t2+= data[i*4+1];
					t3+= data[i*4+2];
				}
				let lightpoll = lightnessScale['s'+decideColor(t1/25, t2/25, t3/25)];
				let vis = 6 - lightpoll;
				document.getElementById("light-pollution").innerHTML = "Brightness of sky in this place is <b style='color: #919eff;'> "+ (22-lightpoll)+
				"mag/arc<sup>2</sup> </b>. Least bright stars visible here have <b style='color: #919eff;'>"+vis+" mag</b>."+
				"<span style='font-weight:800'>(Remember to use slider in settings to set stars brightness correctly)</span>";
				
			};	
		}
	
	}
}

function colorDiffrenceRGB(r1,g1,b1,r2,g2,b2)
{
	return Math.sqrt(Math.pow(r2-r1,2)+Math.pow(g2-g1,2)+Math.pow(b2-b1,2));
}

function decideColor(r,g,b)
{
	if(colorDiffrenceRGB(r,g,b,20,20,20)<60)
	{
		return 1; // "czarny";

	}else if(colorDiffrenceRGB(r,g,b,77,77,77)<60)
	{
		return 2 // "ciemny szary";

	}else if(colorDiffrenceRGB(r,g,b,128,128,128)<60)
	{
		return 3; // "jasny szary";

	}else if(colorDiffrenceRGB(r,g,b,0,37,115)<70)
	{
		return 4; // "ciemny nieb";

	}else if(colorDiffrenceRGB(r,g,b,0,83,189)<40)
	{
		return 5; // "sredni nieb";

	}else if(colorDiffrenceRGB(r,g,b,0,144,221)<140)
	{
		return 6; // "jasny nieb";

	}else if(colorDiffrenceRGB(r,g,b,49,137,15)<110)
	{
		return 7; // "ciemny ziel";

	}else if(colorDiffrenceRGB(r,g,b,160,169,0)<70)
	{
		return 8; // "jasny ziel/zoltawy";

	}else if(colorDiffrenceRGB(r,g,b,228,246,0)<80)
	{
		return 9; // "zolty";

	}else if(colorDiffrenceRGB(r,g,b,255,90,5)<100)
	{
		return 10; // "pomarancz";

	}else if(colorDiffrenceRGB(r,g,b,223,0,0)<180)
	{
		return 11; // "czerwony";

	}else if(colorDiffrenceRGB(r,g,b,227,113,180)<120)
	{
		return 12; // "ciemny roz";

	}else if(colorDiffrenceRGB(r,g,b,225,187,255)<40)
	{
		return 13; // "jasny roz";

	}
	else if(colorDiffrenceRGB(r,g,b,255,255,255)<40)
	{
		return 14; // "biaÅ‚y";

	}else
	{
		console.log(r,g,b)
		return undefined;
	}

	
}


//DATA
let conLatinFullNames= {
	Cep:"Cepheus",
	Cas:"Cassiopeia",
	UMi:"Ursa Minor",
	Dra:"Draco",
	UMa:"Ursa Major",
	Cam:"Camelopardalis",
	Gem:"Gemini",
	Hya:"Hydra",
	Mon:"Monoceros",
	Pyx:"Pyxis",
	Leo:"Leo",
	LMi:"Leo Minor",
	CMi:"Canis Minor",
	Ant:"Antlia",
	Crt:"Crater",
	Cnc:"Cancer",
	Pup:"Puppis",
	Lyn:"Lynx",
	Sex:"Sextans",
	CMa:"Canis Maior",
	Vel:"Vela",
	Cen:"Centaurus",
	Her:"Hercules",
	CrB:"Corona Borealis",
	Crv:"Corvus",
	Ara:"Ara",
	Vir:"Virgo",
	CVn:"Canes Venatici",
	Sco:"Scorpius",
	Lib:"Libra",
	Com:"Coma Berenices",
	Ser1:"Serpens Caput",
	Ser2:"Serpens Cauda",
	Nor:"Norma",
	Oph:"Ophiuchus",
	Lup:"Lupus",
	Boo:"Bootes",
	Del:"Delphinus",
	Ind:"Indus",
	Lac:"Lacerta",
	CrA:"Corona Australis",
	Cap:"Capricornus",
	Vul:"Vulpecula",
	Tel:"Telescopium",
	Lyr:"Lyra",
	Cyg:"Cygnus",
	Mic:"Microscopium",
	Aql:"Aquila",
	Peg:"Pegasus",
	PsA:"Piscis Austrinus",
	Sge:"Sagitta",
	Sgr:"Sagittarius",
	Sct:"Scutum",
	Aqr:"Aquarius",
	Equ:"Equuleus",
	Gru:"Grus",
	And:"Andromeda",
	Ari:"Aries",
	Tau:"Taurus",
	Eri:"Eridanus",
	Phe:"Phoenix",
	Col:"Columba",
	Pic:"Pictor",
	Ori:"Orion",
	Per:"Perseus",
	For:"Fornax",
	Psc:"Pisces",
	Cae:"Caelum",
	Scl:"Sculptor",
	Tri:"Triangulum",
	Cet:"Cetus",
	Aur:"Auriga",
	Lep:"Lepus",
	Hor:"Horologium",
	Cir:"Circinus",
	Men:"Mensa",
	Cha:"Chamaeleon",
	Car:"Carina",
	Cru:"Crux",
	Mus:"Musca",
	Oct:"Octans",
	Pav:"Pavo",
	Aps:"Apus",
	Vol:"Volans",
	Ret:"Reticulum",
	TrA:"Triangulum Australe",
	Tuc:"Tucana",
	Hyi:"Hydrus",
	Dor:"Dorado",
}

let conNumberOfLinks= {
	Cep:0,
	Cas:0,
	UMi:0,
	Dra:0,
	UMa:0,
	Cam:0,
	Gem:0,
	Hya:0,
	Mon:0,
	Pyx:0,
	Leo:0,
	LMi:0,
	CMi:0,
	Ant:0,
	Crt:0,
	Cnc:0,
	Pup:0,
	Lyn:0,
	Sex:0,
	CMa:0,
	Vel:0,
	Cen:0,
	Her:0,
	CrB:0,
	Crv:0,
	Ara:0,
	Vir:0,
	CVn:0,
	Sco:0,
	Lib:0,
	Com:0,
	Ser1:0,
	Ser2:0,
	Nor:0,
	Oph:0,
	Lup:0,
	Boo:0,
	Del:0,
	Ind:0,
	Lac:0,
	CrA:0,
	Cap:0,
	Vul:0,
	Tel:0, 
	Lyr:0,
	Cyg:0,
	Mic:0,
	Aql:0,
	Peg:0,
	PsA:0,
	Sge:0,
	Sgr:0,
	Sct:0,
	Aqr:0,
	Equ:0,
	Gru:0,
	And:0,
	Ari:0,
	Tau:0,
	Eri:0,
	Phe:0,
	Col:0,
	Pic:0,
	Ori:0,
	Per:0,
	For:0,
	Psc:0,
	Cae:0,
	Scl:0,
	Tri:0,
	Cet:0,
	Aur:0,
	Lep:0,
	Hor:0,
	Cir:0,
	Men:0,
	Cha:0,
	Car:0,
	Cru:0,
	Mus:0,
	Oct:0,
	Pav:0,
	Aps:0,
	Vol:0,
	Ret:0,
	TrA:0,
	Tuc:0,
	Hyi:0,
	Dor:0,
}

let sizeOfCons ={
	s0:100,
	s1:8,
	s2:6,
	s3:4,
	s4:0,
}

let linesWithConNames = {
And:[ 12533 , 6860 ],
Tri:[ 11443 , 14055 ],
Ari:[ 17573 , 12929 ],
Psc:[ 222368 , 6186 ],
Cet:[ 4128 , 10700 ],
For:[ 17652 , 12767 ],
Eri:[ 25025 , 23249 ],
Phe:[ 6882 , 3919 ],
Peg:[ 886 , 218045 ],
Per:[ 24760 , 19356 ],
Scl:[ 5737 , 223352 ],
Hor:[ 26967 , 17051 ],
Cep:[ 205021 , 222404 ],
Cas:[ 11415 , 8538 ],
UMi:[ 8890 , 166205 ],
Dra:[ 100029 , 109387 ],
UMa:[ 96833 , 102224 ],
Cam:[ 42818 , 49878 ],
Gem:[ 50019 , 54719 ],
Tau:[ 25204 , 21364 ],
Cir:[ 135379 , 136415 ],
Col:[ 39425 , 36597 ],
Mon:[ 36597 , 55185 ],
Pic:[ 39523 , 39060 ],
CMi:[ 58715 , 61421 ],
Ori:[ 35468 , 30652 ],
Ant:[ 90610 , 82150 ],
Cnc:[ 74739 , 74198 ],
Pup:[ 63700 , 67523 ],
Lyn:[ 70272 , 80081 ],
CMa:[ 54605 , 52089 ],
Aur:[ 40183 , 40312 ],
Lep:[ 33904 , 32887 ],
Cen:[ 109787 , 121263 ],
Hya:[ 85444 , 81797 ],
Crv:[ 108767 , 109379 ],
Pyx:[ 74575 , 75691 ],
CrB:[ 143107 , 141714 ],
Leo:[ 97633 , 87901 ],
LMi:[ 94264 , 90537 ],
Vir:[ 110379 , 107259 ],
CVn:[ 109358 , 112413 ],
Crt:[ 100889 , 99167 ],
Sex:[ 90994 , 90882 ],
Sco:[ 151680 , 149438 ],
Lib:[ 130841 , 135742 ],
Com:[ 108381 , 114710 ],
Nor:[ 147971 , 146686 ],
Lup:[ 143118 , 141556 ],
Boo:[ 124897 , 129174 ],
Vel:[ 93497 , 88955 ],
Del:[ 195810 , 196524 ],
Her:[ 156164 , 158899 ],
Ind:[ 208450 , 202730 ],
Lac:[ 214868 , 211388 ],
CrA:[ 172777 , 170868 ],
Cap:[ 197692 , 193495 ],
Vul:[ 192806 , 189849 ],
Lyr:[ 176437 , 174638 ],
Tel:[ 190421 , 175510 ],
Cyg:[ 197345 , 194093 ],
Mic:[ 203006 , 202627 ],
Ara:[ 152786 , 151249 ],
Aql:[ 191692 , 187929 ],
PsA:[ 216763 , 216956 ],
Sge:[ 189319 , 187076 ],
Sgr:[ 181577 , 175191 ],
Sct:[ 171443 , 173764 ],
Ser1:[ 168723 , 175638 ],
Ser2:[ 141795 , 140573 ],
Oph:[ 149757 , 155125 ],
Aqr:[ 219449 , 216386 ],
Gru:[ 209952 , 214952 ],
Equ:[ 202275 , 202447 ],
Men:[ 32440 , 37763 ],
Cha:[ 93779 , 106911 ],
Car:[ 85123 , 80404 ],
Cru:[ 106490 , 111123 ],
Vol:[ 68520 , 71878 ],
Mus:[ 109668 , 112985 ],
Oct:[ 205478 , 214846 ],
Pav:[ 193924 , 190248 ],
Aps:[ 147675 , 149324 ],
Cae:[ 32831 , 29992 ],
Ret:[ 27442 , 25422 ],
TrA:[  150798  ,  135382  ],
Tuc:[  219571  ,  2884  ],
Hyi:[  16978  ,  24512  ],
Dor:[  29305  ,  27290  ],

}

let lightnessScale = {
	s1:0,
	s2:0.1,
	s3:0.1,
	s4:0.3,
	s5:0.3,
	s6:0.3,
	s7:0.7,
	s8:1,
	s9:1.4,
	s10:2.1,
	s11:2.5,
	s12:3,
	s13:3,
	s14:3.5,
}