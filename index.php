<!DOCTYPE HTML>
  <html lang="pl">
 	 <head>
		<meta charset="utf-8">
		<title>Stars</title>
		
		<meta name="description" content="Stars"/>
		<meta name="keywors" content="programming" />
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge,chrome=1"/>
		<meta http-equiv="Cache-Control" content="no-cache" />

		<link rel="stylesheet" href="style.css" type="text/css" />

		
		
 	</head>
  
	<body>
		<div id="container">
			<header id="header">
				<h1>ASTRO GUIDE</h1>
				<h2>Astronomer's Guide to The Galaxy!</h2>
			</header>
			<main id="main">
				<form id = "wpis-danych">
					<div class="flex-100">
						<h2 class="tytuly">Position and time</h2>
						<h2 class="tytuly">Settings</h2>
					</div>
					<div class="flex-100">
						<div id="miej_czas" class="flex-item">
							Lattitude<br>
							<input type="number" id="lat" value=0><br>
							Longitude<br>
							<input type="number" id="lng" value=0><br>
							<button type="button" id="map-show">Find your position on map</button><br>
							Date and time(in GMT)<br>
							<input id="date" type="date" value="2000-01-01">
							<input id="time" type="time" value="00:00">
							<button type="button" id="now">Set time for now (in GMT)</button><br>
						</div>
						<div id="ustawienia" class="flex-item">
							Minimal star brightness:  <br><span id="star-range-text">5</span> mag<br>
							<input id="star-range" type="range" min="-1.5" max = "6" step="0.1" value="5"><br>
							Star names with brightness at least: <br> <span id="starname-range-text">1</span> mag<br>
							<input id="starname-range" type="range" min="-1.5" max = "3" step="0.1" value="1"><br>
							Constellation names:<br>  <span id="conname-range-text">Medium+</span><br>
							<input id="conname-range" type="range" min="0" max = "4" step="1" value="2"><br>
							<input id="links-check" type="checkbox" checked> - Asterisms lines <br>
							<input id="solarsys-check" type="checkbox" checked> - Solar System<br>
							<input id="ecl-check" type="checkbox" checked> - Ecliptic<br>
							<input id="dso-check" type="checkbox" checked> - Deep space objects<br>
							<input id="stand-check" type="checkbox" unchecked> - Landolt's standard fotometric stars<br><br>
							ADVANCED<br>
							Calculator for Landolt's stars<br>
							Enter star's name according to database: <a href = "http://www.eso.org/sci/observing/tools/standards/Landolt.html" style="color:white">database</a>:<br>
							<input id="stand-name"><br>
							<button type="button" id='stand-button'>Calculate star's position!</button>
							<div id = "stand-display"></div>
							
						</div>
					</div>
					<span id="show" style="display:none">
						<div id="map" ></div>
					</span>
					<div class="flex-100">
						<button type="button" id="submit">Show the map!</button>
					</div>
				</form>

				
				
				<span id="canvas-parent" style="display:none">
					<center><canvas id="canvas"></canvas></center>
					<div class="flex-100">
						<h2 class="tytuly" style="text-decoration: underline;">Clouds</h2>
						<h2 class="tytuly" style="text-decoration: underline;">Light pollution</h2>
					</div>
					<div class="flex-100">
						<div class="flex-item" id="weather"></div><br>
						<div class="flex-item" id="light-pollution"></div><br>
					</div>	
				</span>
				
			</main>
		</div>

		<canvas id="test" style="display:none">
		</canvas>
		<script type="text/javascript" src="main.js"></script>
		<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUUR3rmebpIm98TxMmXkPZy6wjn2IOfV8&callback=initMap">
		</script>	 
	</body>
  
</html>
